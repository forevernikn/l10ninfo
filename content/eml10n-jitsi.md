---
title: EngageMedia Localization Jitsi
aliases:
- /eml10n-bbb/
---

[L10N.PARTY](https://l10n.party) is a self-hosted [Jitsi](https://jitsi.org/) instance maintained by the EngageMedia team for its localization community.

This Jitsi instance is hosted in Singapore and is available as a resource to EngageMedia contributors to organise virtual localization meetups, sprints, and other meetings.

The previous BigBlueButton meeting links (`bbb.l10n.info`) will automatically redirect to `l10n.party`.

The Jitsi site uses Cloudflare Security to prevent bots. Users will usually see an intersitial page that perform a number of checks on the site connection to the browser.

![Cloudflare interstitial page](/images/l10nparty-cloudflare-interstitial-page.jpg)
